package com.example.demo.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.example.demo.models.entity.Perfil;
import com.example.demo.models.entity.Usuario;
import com.example.demo.models.services.IUploadFileService;
import com.example.demo.models.services.IUsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/sitiapp")
public class UsuarioRestController {
    @Autowired
    private IUsuarioService usuarioService;

    @Autowired
    private IUploadFileService uploadService;
    
    @Autowired
	private BCryptPasswordEncoder passwordEncoder;
    
    @Operation(summary = "Retorna una lista con todos los usuarios")
    @Secured("ROLE_ADMIN") 
    @GetMapping("/usuarios")
    public List<Usuario> index() {
        return usuarioService.findAll();
    }

    @Operation(summary = "Retorna un page de usuarios")
    @Secured("ROLE_ADMIN") 
    @GetMapping("/usuarios/page/{page}")
    public Page<Usuario> index(@PathVariable Integer page) {
        Pageable pageable = PageRequest.of(page,5);
        return usuarioService.findAll(pageable);
    }

    @Secured("ROLE_ADMIN") 
    @GetMapping("/usuarios/page/{page}/{term}")
	@ResponseStatus(HttpStatus.OK)
	public Page<Usuario> filtrarusuario(@PathVariable String term, @PathVariable Integer page){
        Pageable pageable = PageRequest.of(page, 5);
		return usuarioService.findUsuariosByUsername(term, pageable);
	}

    @Secured("ROLE_ADMIN") 
    @GetMapping("/usuarios/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Usuario usuario = null;
        Map<String, Object> response = new HashMap<>();
        try {
            usuario = usuarioService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (usuario == null) {
            response.put("mensaje", "¡El usuario ID: ".concat(id.toString()).concat(" no existe en la base de datos!"));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
    }


     @Secured("ROLE_ADMIN")
     @PostMapping("/usuarios")
     public ResponseEntity<?> create(@Valid @RequestBody Usuario usuario, BindingResult result) {
         Usuario newusuario = null;
         Map<String, Object> response = new HashMap<>();
 
         if (result.hasErrors()) {
             List<String> errors = result.getFieldErrors()
                     .stream()
                     .map(err -> "El campo: " + err.getField() + " // " + err.getDefaultMessage())
                     .collect(Collectors.toList());
             response.put("errors", errors);
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
         }
 
         try {
            String passwordBcrypt = passwordEncoder.encode(usuario.getPassword());
            usuario.setPassword(passwordBcrypt);
            newusuario = usuarioService.save(usuario);
         } catch (DataAccessException e) {
             response.put("mensaje", "Error al realizar el insert en la base de datos");
             response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
 
         }
         response.put("mensaje", "El usuario ha sido creado con éxito!");
         response.put("usuario", newusuario);
         return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
     }

     @Secured("ROLE_ADMIN")
     @PutMapping("/usuarios/{id}")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<?> update(@Valid @RequestBody Usuario usuario, BindingResult result, @PathVariable Long id) {
         Usuario usuarioActual = usuarioService.findById(id);
         Usuario usuarioUpdate = null;
 
         Map<String, Object> response = new HashMap<>();
 
         if (result.hasErrors()) {
             List<String> errors = result.getFieldErrors()
                     .stream()
                     .map(err -> "El campo: " + err.getField() + " // " + err.getDefaultMessage())
                     .collect(Collectors.toList());
             response.put("errors", errors);
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
         }
 
         if (usuarioActual == null) {
             response.put("mensaje", "Error: no se pudo editar , el usuario ID: ".concat(id.toString())
                     .concat(" no existe en la base de datos!"));
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
         }
 
         try {
             usuarioActual.setNombre(usuario.getNombre());
             usuarioActual.setApellido(usuario.getApellido());
             usuarioActual.setPerfil(usuario.getPerfil());
             usuarioUpdate = usuarioService.save(usuarioActual);
 
         } catch (DataAccessException e) {
             response.put("mensaje", "Error al actualizar el usuario en la base de datos");
             response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
 
         }
 
         response.put("mensaje", "El usuario ha sido actualizado con éxito!");
         response.put("usuario", usuarioUpdate);
         return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
 
     }

     @Secured("ROLE_ADMIN")
     @PostMapping("/usuarios/uploads")
     public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
         Map<String, Object> response = new HashMap<>();
         Usuario usuario = usuarioService.findById(id);
 
         if (!archivo.isEmpty()) {
             String nombreArchivo=null;
             try {
                 nombreArchivo=uploadService.copiar(archivo);
             } catch (IOException e) {
                 response.put("mensaje", "Error al subir la imagen ");
                 response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
                 return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
             }
 
             String nombreFotoAnterior = usuario.getFoto();
             uploadService.eliminar(nombreFotoAnterior);//Eliminar foto anterior si ya tenia una
         
             usuario.setFoto(nombreArchivo);
             usuarioService.save(usuario);
             response.put("usuario", usuario);
             response.put("mensaje", "Imagen subida con éxito: " + nombreArchivo);
         }
         return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
 
     }

    @GetMapping("/usuarios/uploads/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
        
        Resource recurso = null;
        try {
           recurso = uploadService.cargar(nombreFoto);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //para que se pueda descargar 
        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+ recurso.getFilename()+"\"");
        return new ResponseEntity<Resource>(recurso,cabecera,HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/usuarios/tipos_perfiles")
    public List<Perfil> listarTipoPerfiles() {
        return usuarioService.findAlltiposPerfiles();
    }

}
