package com.example.demo.controllers;

import java.util.List;

import com.example.demo.models.entity.Factura;
import com.example.demo.models.entity.Producto;
import com.example.demo.models.services.IClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/sitiapp")
public class FacturaRestController {
    
    @Autowired
    private IClienteService clienteService;

    @Operation(summary = "Retorna una factura buscada por su id")
    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @GetMapping("/facturas/{id}")
    @ResponseStatus(HttpStatus.OK) 
    public Factura show(@PathVariable Long id){
         return clienteService.findFacturaById(id);
    }
    @Operation(summary = "Eliminar una factura buscada por su id")
    @Secured("ROLE_ADMIN")
    @DeleteMapping("/facturas/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) 
    public void delete(@PathVariable Long id){
        clienteService.deleteFacturaById(id);
    }

    @Operation(summary = "filtrar productos por un nombre")
    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @GetMapping("/facturas/filtrar-productos/{term}")
    @ResponseStatus(HttpStatus.OK)
    public List<Producto> filtrarProductos(@PathVariable String term){
       return clienteService.findProductoByNombre(term);
    }

    @Operation(summary = "Crear factura")
    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @PostMapping("/facturas")
    @ResponseStatus(HttpStatus.CREATED)
    public Factura crear(@RequestBody Factura factura){
      return clienteService.saveFactura(factura);
    }


}
