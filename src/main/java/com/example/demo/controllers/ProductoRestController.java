package com.example.demo.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.example.demo.models.entity.Producto;
import com.example.demo.models.services.IProductoService;
import com.example.demo.models.services.IUploadFileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;

import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/sitiapp")
public class ProductoRestController {
    @Autowired
    private IProductoService productoService;

    @Autowired
    private IUploadFileService uploadService;

    @Operation(summary = "Retorna una lista con todos los productos")
    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @GetMapping("/productos")
    public List<Producto> index() {
        return productoService.findAll();
    }

    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @GetMapping("/productos/page/{page}")
    public Page<Producto> index(@PathVariable Integer page) {
        Pageable pageable = PageRequest.of(page,5);
        return productoService.findAll(pageable);
    }

    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
    @GetMapping("/productos/page/{page}/{term}")
	@ResponseStatus(HttpStatus.OK)
	public Page<Producto> filtrarProductos(@PathVariable String term, @PathVariable Integer page){
        Pageable pageable = PageRequest.of(page, 5);
		return productoService.findProductoByNombre(term, pageable);
	}

    
    @Secured({"ROLE_ADMIN","ROLE_CAJERO"})
     @GetMapping("/productos/{id}")
     public ResponseEntity<?> show(@PathVariable Long id) {
         Producto producto = null;
         Map<String, Object> response = new HashMap<>();
         try {
             producto = productoService.findById(id);
         } catch (DataAccessException e) {
             response.put("mensaje", "Error al realizar la consulta en la base de datos");
             response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
         }
 
         if (producto == null) {
             response.put("mensaje", "¡El producto ID: ".concat(id.toString()).concat(" no existe en la base de datos!"));
             return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
         }
         return new ResponseEntity<Producto>(producto, HttpStatus.OK);
     }

    @Secured("ROLE_ADMIN")
    @PostMapping("/productos")
    public ResponseEntity<?> create(@Valid @RequestBody Producto producto, BindingResult result) {
        Producto newproducto = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(err -> "El campo: " + err.getField() + " // " + err.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            newproducto = productoService.save(producto);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
        response.put("mensaje", "El producto ha sido creado con éxito!");
        response.put("producto", newproducto);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }


    @Secured("ROLE_ADMIN")
    @PutMapping("/productos/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> update(@Valid @RequestBody Producto producto, BindingResult result, @PathVariable Long id) {
        Producto productoActual = productoService.findById(id);
        Producto productoUpdate = null;

        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(err -> "El campo: " + err.getField() + " // " + err.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        if (productoActual == null) {
            response.put("mensaje", "Error: no se pudo editar , el producto ID: ".concat(id.toString())
                    .concat(" no existe en la base de datos!"));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        try {
            productoActual.setNombre(producto.getNombre());
            productoActual.setValor_unitario(producto.getValor_unitario());
            productoActual.setEstado(producto.getEstado());
            productoUpdate = productoService.save(productoActual);

        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar el producto en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }

        response.put("mensaje", "El producto ha sido actualizado con éxito!");
        response.put("producto", productoUpdate);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    
    @Secured({"ROLE_ADMIN"})
    @PostMapping("/productos/uploads")
    public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
        Map<String, Object> response = new HashMap<>();
        Producto producto = productoService.findById(id);

        if (!archivo.isEmpty()) {
            String nombreArchivo=null;
            try {
                nombreArchivo=uploadService.copiar(archivo);
            } catch (IOException e) {
                response.put("mensaje", "Error al subir la imagen ");
                response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            String nombreFotoAnterior = producto.getFoto();
            uploadService.eliminar(nombreFotoAnterior);//Eliminar foto anterior si ya tenia una
        
            producto.setFoto(nombreArchivo);
            productoService.save(producto);
            response.put("Producto", producto);
            response.put("mensaje", "Imagen subida con éxito: " + nombreArchivo);
        }
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    
    @GetMapping("/productos/uploads/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
        
        Resource recurso = null;
        try {
           recurso = uploadService.cargar(nombreFoto);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //para que se pueda descargar 
        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+ recurso.getFilename()+"\"");
        return new ResponseEntity<Resource>(recurso,cabecera,HttpStatus.OK);
    }
    
}
