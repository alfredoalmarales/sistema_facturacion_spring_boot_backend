package com.example.demo.auth;

public class JwtConfig {
    public static final String LLAVE_SECRETA ="2021septiembre3" ;

    public static final String RSA_PRIVADA="-----BEGIN RSA PRIVATE KEY-----\r\n"+
    "MIIEowIBAAKCAQEA4ImFmApTTxEaDk1UHuX5nWrRmu5MoX+PCDQcj4Yvej1o4Niy\r\n"+
    "x/9djANhVon2IK7Hr62X8yClLZHp+cqGnmG28I2tGI8uF4Qwk6quawEouhrWi69m\r\n"+
    "gHeV9qOjw9UpQHq4aLJCu47O6j9P8yEXBgcm0XzdaRFPr4LwJvHdw6xyjVfn+YRr\r\n"+
    "dfZ9/o8nYlp9X45jdglcdAJOFlOU3So/V5H+CNh+vAgMxIB2A70isp432RUQHaUN\r\n"+
    "jQa/aC7VhzEOqcYWZ2Rq4AO44eCmgF9LRlzGuhbJwIrwUERM54VhjpA0LP2USfPM\r\n"+
    "eckPyIxmN50hTl2H28Pz/wV1u9zk3A09cyNZxwIDAQABAoIBAE6ct1Io40v+X9H8\r\n"+
    "DuSWfdM4T9RfdCLixxAHNqylPUAd+fcmx0kwRe88S7sBqFN+/diq/VUY69vNh052\r\n"+
    "MrWG3obQA3diwa9oD53TlydNi5XEybK0IT3/IFjup+prWfsCPEgjiSujbxwiOHqV\r\n"+
    "7jqlDFkxz072yaBYRn5MFelpGd4T7ev4020fZ22EQbgyL9dBIN56XWKPdaURLEJx\r\n"+
    "a3lbWHJcSZGyp2+qQdyrho4+pFY4p0gjopD2Y49gwkNyxzG/Q+yzDqKh7c/6d78Z\r\n"+
    "bhT4htR3x4ggkWyBFWLSbHThY8vpPc6dpz9SHkbJMhBuLX5tXEvTt3z+pMazsmTj\r\n"+
    "3XQ9VtECgYEA9cC/pUwjaTUseh1f4PHfnDJnXEFHOGBDP4rjB+XjZuzY18oDYFW9\r\n"+
    "NOhxk9RwJCBBs30jfTkkTowUIPCDj45iuV/689E+cu1/cyGdcG3s5iWxoUTVtsSI\r\n"+
    "mnslMywNHWJ4Dj93x4W/o9QaKp/hfHzO2AYOOidkRM+6jYaoVZ9rfN8CgYEA6eZP\r\n"+
    "KZid2C6eEx4hg/eVw0Nfo8jc2A0UhlPmSNRQGlTbt0HbKAzIyySsiH/vArKfVKEw\r\n"+
    "B+cosP5UrooDkQT6wgjiidD3DSTD1Sc0OXQMtw2N+2X74GwHuQeHVu0m71M0+8iY\r\n"+
    "t4ulkexw+USQrbnzVpkdjPAppSoeJ/nqAZC42BkCgYEAh5ccZhg3JXIIIywX3WW2\r\n"+
    "SVjp/FWr9Ofv0pxPnOjy6bPeBFlaOYm0xf9Fwxmf8d62QVDXBt1DMe9Jn87z133o\r\n"+
    "7HyPWnzlZsWw3xX55rFP1r+PXPo8rAWNlSZCpRQ1/DN7J+lOrhAWZ068TS4/W88E\r\n"+
    "1N2qdd2MDgdK15EnSW6XAKMCgYB5OFg8mZDskjRwegQqrazbNtahPp9eBWFmwKKv\r\n"+
    "igE9tE7uqd3X4tBJn+cfExae0k4LAUKwRdfr6tOeZX61sNv4pbwqarcvA6fOl3PU\r\n"+
    "dtWEJS05ZkBkrfhOp503qs6r5/akqvCi52fWubBPL36/2ohHkDElcu5SYAKi+z2Q\r\n"+
    "tB1oOQKBgFKQ1F5h3Y7/touO9L4kWXeNfY9ObwfBNGW77XyOd94q8ePMxltNCtZ0\r\n"+
    "O1LdLlkWUBv0+cEkUyuLBuNgwZD6qorhZxjyns4cfWG4chjr9c2RZ/dGWxeMdAA/\r\n"+
    "WYewIZ41VXyxtpdleVbZ363Vh2NH3sMZoZTxdbT5yRnP1p5Ak3L9\r\n"+
    "-----END RSA PRIVATE KEY-----";
    
    public static final String RSA_PUBLICA="-----BEGIN PUBLIC KEY-----\r\n"+
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4ImFmApTTxEaDk1UHuX5\r\n"+
    "nWrRmu5MoX+PCDQcj4Yvej1o4Niyx/9djANhVon2IK7Hr62X8yClLZHp+cqGnmG2\r\n"+
    "8I2tGI8uF4Qwk6quawEouhrWi69mgHeV9qOjw9UpQHq4aLJCu47O6j9P8yEXBgcm\r\n"+
    "0XzdaRFPr4LwJvHdw6xyjVfn+YRrdfZ9/o8nYlp9X45jdglcdAJOFlOU3So/V5H+\r\n"+
    "CNh+vAgMxIB2A70isp432RUQHaUNjQa/aC7VhzEOqcYWZ2Rq4AO44eCmgF9LRlzG\r\n"+
    "uhbJwIrwUERM54VhjpA0LP2USfPMeckPyIxmN50hTl2H28Pz/wV1u9zk3A09cyNZ\r\n"+
    "xwIDAQAB\r\n"+
    "-----END PUBLIC KEY-----";
}
