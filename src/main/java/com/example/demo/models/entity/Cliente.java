package com.example.demo.models.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="`CLIENTES`")
public class Cliente  implements Serializable {
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 100)
	private String identificacion;

	@NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 100)
	private String razon_social;

	@Column(name="fecha_registro")
	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@Column(nullable=false,length = 1)
	private String estado;

	private String foto;
	
	@JsonIgnoreProperties(value={"cliente","hibernateLazyInitializer","handler"},allowSetters = true) 
	@OneToMany( fetch = FetchType.LAZY, mappedBy = "cliente",cascade = CascadeType.ALL)
	private List<Factura> facturas;

	@NotNull(message = "el tipo de identificación no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tipo_identificacion")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Tipo_Identificacion tipo_identificacion;

	public Cliente(){
		this.facturas = new ArrayList<>();
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Factura> getFacturas() {
		return this.facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
	public String getRazon_social() {
		return this.razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public Date getFecha_registro() {
		return this.fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	
	public String getFoto() {
		return foto;
	}
	public String getESTADO() {
		return this.estado;
	}

	public void setESTADO(String ESTADO) {
		this.estado = ESTADO;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getIDENTIFICACION() {
		return this.identificacion;
	}

	public void setIDENTIFICACION(String IDENTIFICACION) {
		this.identificacion = IDENTIFICACION;
	}


	public Tipo_Identificacion getTipo_identificacion() {
		return this.tipo_identificacion;
	}

	public void setTipo_identificacion(Tipo_Identificacion tipo_identificacion) {
		this.tipo_identificacion = tipo_identificacion;
	}

	private static final long serialVersionUID = 1L; 
}
