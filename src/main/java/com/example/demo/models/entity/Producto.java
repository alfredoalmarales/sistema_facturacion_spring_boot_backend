package com.example.demo.models.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PRODUCTOS")
public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 100)
    private String nombre;

    @NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 45)
    private String estado;

    @NotNull(message ="no puede estar vacio")
	@Column(nullable=false)
    private Integer valor_unitario;

    @Column(name="fecha_registro")
	private Date fecha_registro;
    
    private String foto;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getValor_unitario() {
        return this.valor_unitario;
    }

    public void setValor_unitario(Integer valor_unitario) {
        this.valor_unitario = valor_unitario;
    }
    public Date getFecha_registro() {
		return this.fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

	private static final long serialVersionUID = 1L;

}
