package com.example.demo.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;


@Entity
@Table(name="TIPOS_IDENTIFICACIONES")
public class Tipo_Identificacion implements Serializable {
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 3)
    private String ABREVIATURA;

	@NotEmpty(message ="no puede estar vacio")
	@Column(nullable=false,length = 100)
    private String DESCRIPCION;
    
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getABREVIATURA() {
        return this.ABREVIATURA;
    }

    public void setABREVIATURA(String ABREVIATURA) {
        this.ABREVIATURA = ABREVIATURA;
    }

    public String getDESCRIPCION() {
        return this.DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

	private static final long serialVersionUID = 1L;

}
