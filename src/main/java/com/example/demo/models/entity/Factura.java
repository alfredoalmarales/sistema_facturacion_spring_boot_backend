package com.example.demo.models.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "FACTURAS")
public class Factura implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long CONSECUTIVO;

    @JsonIgnoreProperties(value={"facturas","hibernateLazyInitializer","handler"},allowSetters =true )
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_cliente")
    private Cliente cliente;

    @Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;

    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="factura_id")
    private List<FacturaDetalle> facturadetalle;

    public Integer getTotal(){
        Integer total = 0;
        for(FacturaDetalle item: facturadetalle){
            total += item.getImporte();
        }
        return total;
   }

    public List<FacturaDetalle> getFacturadetalle() {
        return this.facturadetalle;
    }


    public void setFacturadetalle(List<FacturaDetalle> facturadetalle) {
        this.facturadetalle = facturadetalle;
    }

    public Factura(){
        facturadetalle= new ArrayList<>();
    }

    @PrePersist
    public void prePersist(){
        this.createAt = new Date();
    }
    public Long getCONSECUTIVO() {
        return this.CONSECUTIVO;
    }

    public void setCONSECUTIVO(Long CONSECUTIVO) {
        this.CONSECUTIVO = CONSECUTIVO;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getCreateAt() {
        return this.createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

	private static final long serialVersionUID = 1L;

}
