package com.example.demo.models.dao;

import java.util.List;

import com.example.demo.models.entity.Cliente;
import com.example.demo.models.entity.Tipo_Identificacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IClienteDao extends JpaRepository<Cliente, Long> {
    
    @Query("from Tipo_Identificacion")
    public List<Tipo_Identificacion> findAllTipo_Identificaciones();
    
    @Query("select c from Cliente c where c.razon_social like %?1%")
    public Page<Cliente> findByNombre(String term, Pageable pageable);
                                                                                        
    @Query( value="select * from clientes c inner join tipos_Identificaciones t ON c.tipo_identificacion = t.id  WHERE t.id =:tipoid AND c.identificacion=:iden",nativeQuery = true)
    public Cliente find(@Param("tipoid") Long tipoid , @Param("iden") String iden);
}    
