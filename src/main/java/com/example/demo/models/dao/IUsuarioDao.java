package com.example.demo.models.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import com.example.demo.models.entity.Perfil;
import com.example.demo.models.entity.Usuario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUsuarioDao extends JpaRepository<Usuario,Long>{
    
    public Usuario findByUsername(String usuario);

    @Query("select u from Usuario u where u.username like %?1%")
    public Page<Usuario> findByUserName(String term, Pageable pageable);
    
    @Query("from Perfil")
    public List<Perfil> findAllTipoPerfiles();
    

}
