package com.example.demo.models.services;

import java.util.List;

import com.example.demo.models.entity.Producto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IProductoService {
    public List<Producto> findAll();
    
	public Page<Producto> findAll(Pageable pageable);

    public Producto findById(Long id);

    public Producto save(Producto producto);

	public void delete(Long id);

	public Page<Producto> findProductoByNombre(String term,Pageable pageable);

}
