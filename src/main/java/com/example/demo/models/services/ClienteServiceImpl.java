package com.example.demo.models.services;

import java.util.List;

import com.example.demo.models.dao.IClienteDao;
import com.example.demo.models.dao.IFacturaDao;
import com.example.demo.models.dao.IProductoDao;
import com.example.demo.models.entity.Cliente;
import com.example.demo.models.entity.Factura;
import com.example.demo.models.entity.Producto;
import com.example.demo.models.entity.Tipo_Identificacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
	  private IClienteDao clienteDao;
    
    @Autowired
    private IFacturaDao facturaDao;

    @Autowired
    private IProductoDao productoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return (List<Cliente>) clienteDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Cliente> findAll(Pageable pageable) {
      return clienteDao.findAll(pageable);
    }

   

    @Override
    @Transactional(readOnly = true)
    public Cliente findById(Long id) {
		return clienteDao.findById(id).orElse(null);       
    }

    @Override
    public Cliente find(Long tipoid, String iden) {
      return clienteDao.find(tipoid,iden);
    }
  
    @Override
    public Cliente save(Cliente cliente) {
		return clienteDao.save(cliente);
    }

    @Override
    public void delete(Long id) {
		clienteDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Factura findFacturaById(Long id) {
      return facturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Factura saveFactura(Factura factura) {
      return facturaDao.save(factura);
    }

    @Override
    @Transactional
    public void deleteFacturaById(Long id) {
      facturaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findProductoByNombre(String term) {
      return productoDao.findByNombreContainingIgnoreCase(term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tipo_Identificacion> findAllTipo_Identificaciones() {
      return clienteDao.findAllTipo_Identificaciones();
    }

	@Override
	public Page<Cliente> findClienteByNombre(String term, Pageable pageable) {
		return clienteDao.findByNombre(term,pageable);
	}   
}
