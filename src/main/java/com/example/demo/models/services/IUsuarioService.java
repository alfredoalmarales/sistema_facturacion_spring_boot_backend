package com.example.demo.models.services;


import java.util.List;

import com.example.demo.models.entity.Perfil;
import com.example.demo.models.entity.Usuario;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Page;
public interface IUsuarioService {

	public Usuario findById(Long id);

	public Page<Usuario> findAll(Pageable pageable);

	public List<Usuario> findAll();

	public Usuario findByUsername(String username);
    
	public Page<Usuario> findUsuariosByUsername(String term,Pageable pageable);

	public Usuario save(Usuario usuario);

	public List<Perfil> findAlltiposPerfiles();


}
