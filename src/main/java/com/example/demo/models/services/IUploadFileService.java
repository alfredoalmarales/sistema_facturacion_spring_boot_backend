package com.example.demo.models.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IUploadFileService {
    //Cargar imagen
    public Resource cargar(String nombreFoto) throws MalformedURLException;
    //Copiamos la imagen
    public String copiar(MultipartFile archivo) throws IOException;
    //Eliminar imagen
    public boolean eliminar(String nombreFoto);
    //Obtener la ruta donde guardaremos el archivo
    public Path getPath(String nombreFoto);
}
