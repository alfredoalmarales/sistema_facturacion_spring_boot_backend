package com.example.demo.models.services;

import java.util.Arrays;
import java.util.List;

import com.example.demo.models.dao.IUsuarioDao;
import com.example.demo.models.entity.Perfil;
import com.example.demo.models.entity.Usuario;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UsuarioServiceImpl implements IUsuarioService,UserDetailsService {
    
    private Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);
    @Autowired
    private IUsuarioDao usuarioDao;
    @Override
    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario=usuarioDao.findByUsername(username);
        if(usuario==null){
            logger.error("No existe el usuario'"+username+"'en el sistema");
            throw new UsernameNotFoundException("No existe el usuario'"+username+"'en el sistema");
        }

        List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(usuario.getPerfil().getNombre()));
        return new User(usuario.getUsername(),usuario.getPassword(),usuario.getEnabled(), true, true, true,authorities);
         
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario findById(Long id) {
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
	@Transactional(readOnly=true)
	public Usuario findByUsername(String username) {
		return usuarioDao.findByUsername(username);
	}

    @Override
    public Page<Usuario> findUsuariosByUsername(String term, Pageable pageable) {
        return usuarioDao.findByUserName(term,pageable);
    }

    @Override
	@Transactional(readOnly=true)
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    public Page<Usuario> findAll(Pageable pageable) {
        return usuarioDao.findAll(pageable);
    }

    @Override
    public Usuario save(Usuario usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    public List<Perfil> findAlltiposPerfiles() {
        return usuarioDao.findAllTipoPerfiles();
    }

    
}
