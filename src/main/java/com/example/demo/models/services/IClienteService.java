package com.example.demo.models.services;

import java.util.List;

import com.example.demo.models.entity.Cliente;
import com.example.demo.models.entity.Factura;
import com.example.demo.models.entity.Producto;
import com.example.demo.models.entity.Tipo_Identificacion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IClienteService {
    public List<Cliente> findAll();
	
	public Page<Cliente> findAll(Pageable pageable);
	

	public Cliente findById(Long id);

	public Cliente find(Long tipoid,String iden);
	
	public Cliente save(Cliente cliente);
	
	public void delete(Long id);

	public Page<Cliente> findClienteByNombre(String term,Pageable pageable);

	public List<Tipo_Identificacion> findAllTipo_Identificaciones();

	public Factura findFacturaById(Long id);

	public Factura saveFactura(Factura factura);

	public void deleteFacturaById(Long id);

	public List<Producto> findProductoByNombre(String term);

    
}
