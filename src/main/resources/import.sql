
-- -- Tabla Perfil
INSERT INTO perfil (nombre) VALUES("ROLE_ADMIN");
INSERT INTO perfil (nombre) VALUES("ROLE_CAJERO");

-- -- Tabla Usuarios

-- INSERT INTO usuario (username,password,enabled,nombre,apellido,id_perfil) VALUES("juanpe","$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq",1,"Juan","Perez",1);
-- INSERT INTO usuario (username,password,enabled,nombre,apellido,id_perfil) VALUES("pepe19","$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK",1,"Pepe","Mojica",2);
-- INSERT INTO usuario (username,password,enabled,nombre,apellido,id_perfil) VALUES("maria20","$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK",1,"maria","castro",2);
INSERT INTO usuario (nombre,apellido,username,password,enabled,id_perfil,fecha_registro,foto) VALUES("juan","perez","juanpe","$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq",1,1,"2022-01-15","usuario1.jpg");
INSERT INTO usuario (nombre,apellido,username,password,enabled,id_perfil,fecha_registro,foto) VALUES("maria","rodrigez","maria20","$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq",1,2,"2022-01-16","usuario2.jpg");
INSERT INTO usuario (nombre,apellido,username,password,enabled,id_perfil,fecha_registro,foto) VALUES("diego","montaner","diegomonta","$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq",1,1,"2022-04-07","usuario3.jpg");
INSERT INTO usuario (nombre,apellido,username,password,enabled,id_perfil,fecha_registro,foto) VALUES("andres","castro","catroandres","$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq",1,1,"2022-01-27","usuario4.jpg");



-- Tabla tipos de identificaciones
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(1,"CC","Cédula de ciudadanía");
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(2,"CX","Cédula de extrangería");
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(3,"PS","Pasaporte");
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(4,"NT","Número de identificación Tributaria");
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(5,"IX","Número de identificación Tributaria en el país de origen");
INSERT INTO tipos_identificaciones (id,abreviatura,descripcion) VALUES(6,"X","No especificado");


-- -- Tabla Clientes
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado,foto) VALUES(1,"123","Juan perez","2022-01-12","1","cliente1.jpg");
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado,foto) VALUES(2,"1234","coca cola","2022-01-10","1","cliente2.png");
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado,foto) VALUES(4,"666","andres montaner","2022-01-09","1","cliente3.jpg");
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado) VALUES(5,"777","empresa pepe","2022-01-08","1");
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado) VALUES(6,"888","venezolano chan","2022-01-07","1");
INSERT INTO clientes (tipo_identificacion,identificacion,razon_social,fecha_registro,estado) VALUES(2,"123","prueba","2022-01-07","1");



-- INSERT INTO clientes (nombre,apellido,email,fecha_registro) VALUES("Juan","Perez","juanpe@gmail.com","2022-01-03");
-- INSERT INTO clientes (nombre,apellido,email,fecha_registro) VALUES("Pepe","Mojica","pepe@gmail.com","2022-01-03");
-- INSERT INTO clientes (nombre,apellido,email,fecha_registro) VALUES("nelson","almarales","nelson@gmail.com","2022-01-11");

-- -- Tabla productos
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("celular","activo",700000,"2022-01-10","producto8.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("coca cola","activo",2000,"2022-01-11","producto1.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("gafas","activo",5000,"2022-01-12","producto4.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("tv","activo",120000,"2022-01-13","producto5.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("pan","activo",1000,"2022-01-14","producto2.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("portatil","agotado",2300000,"2022-01-14","producto9.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("camisa","activo",73000,"2022-08-19","producto7.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro,foto) VALUES("arroz","activo",1500,"2022-03-16","producto3.jpg");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro) VALUES("panela","activo",1200,"2022-02-18");
INSERT INTO productos (nombre,estado,valor_unitario,fecha_registro) VALUES("sal","activo",1700,"2022-02-17");





-- -- Facturas y facturas detalles
INSERT INTO facturas (id_cliente,create_at) VALUES(1,"2022-01-01");
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(1,1,2);
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(1,2,3);
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(1,5,5);


INSERT INTO facturas (id_cliente,create_at) VALUES(2,"2022-01-02");
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(2,3,1);
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(2,4,6);
INSERT INTO facturas_detalle (factura_id,producto_id,cantidad) VALUES(2,1,2);


